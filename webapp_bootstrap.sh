#!/usr/bin/env bash


# create project folder
#sudo -v -p mkdir /var/www/
sudo chmod -v -R 777 /var/www/

sudo chgrp vagrant /var/www
sudo chmod /var/www
sudo cp ./consul1/webapp.php /var/www

sudo chgrp vagrant /etc/init
sudo chmod /etc/init
sudo cp ./consul.conf /etc/init/consul.conf


# update / upgrade
sudo apt-get update
sudo apt-get -y upgrade 
sudo apt-get install -y curl unzip

# install Consul
cd /tmp
wget https://releases.hashicorp.com/consul/0.6.4/consul_0.6.4_linux_amd64.zip
echo Installing Consul...
sudo unzip -o consul_0.6.4_linux_amd64.zip
sudo chmod +x consul
mv -v consul /usr/bin/consul
sudo chmod -R 777 /usr/bin/consul


# create consul directories
sudo mkdir -v -p /etc/consul.d
sudo chmod -v -R 777 /etc/consul.d
sudo mkdir -v -p /tmp/consul
sudo chmod -v -R 777 /tmp/consul

# install apache 2.5 and php 5.5
sudo apt-get install -y apache2
sudo apt-get install -y php5

sudo apt-get install tasksel
sudo tasksel install lamp-server

# setup hosts file
VHOST=$(cat <<EOF
<VirtualHost *:80>
    DocumentRoot "/var/www/${PROJECTFOLDER}"
    <Directory "/var/www/${PROJECTFOLDER}">
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>
EOF
)

# enable mod_rewrite
sudo a2enmod rewrite

# restart apache
service apache2 restart

# install git
sudo apt-get -y install git

# install Composer
curl -s https://getcomposer.org/installer | php
mv -v composer.phar /usr/local/bin/composer

# Start Consul
exec consul agent -config-file=/etc/consul.d/config.json