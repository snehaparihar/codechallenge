
sudo chgrp vagrant /etc/init
sudo chmod /etc/init
sudo cp ./consul.conf /etc/init/consul.conf
echo Installing dependencies...
# update/upgrade
sudo apt-get update
sudo apt-get install -y unzip curl

# install dnsmasq
sudo apt-get install -y dnsmasq

# install mysql
sudo apt-get install -y mysql-server

# install Consul
cd /tmp
wget https://releases.hashicorp.com/consul/0.6.4/consul_0.6.4_linux_amd64.zip
echo Installing Consul...
sudo unzip -o consul_0.6.4_linux_amd64.zip
sudo chmod +x consul
mv -v consul /usr/bin/consul
sudo chmod -R 777 /usr/bin/consul


# create consul directories
sudo mkdir -v -p /etc/consul.d
sudo chmod -v -R 777 /etc/consul.d
sudo mkdir -v -p /tmp/consul
sudo chmod -v -R 777 /tmp/consul




# Start Consul
exec consul agent -config-file=/etc/consul.d/config.json
