# README #

## Docker orchestration, service discovery, Vagrant ##
 
## Overview ##
This repository contains files for dockerizing a small PHP web application which on request retrieves several rows of data from the database MySQL.
 
This application resolve database configuration using the service discovery tool Consul.

Using Vagrant, 3 nodes are configured for web app, database and consul client respectively. Consul agent is running in server mode on web app node and database node, and in client mode on consul client node.

## Prerequisites ##

You will need to have installed Vagrant and VirtualBox.

## Usage ##

Once you have downloaded the files open a command prompt and change to the directory containing the Vagrantfile. To start the bootstrap server type the following:

`vagrant up consul1`

Thereafter you can start the db server:

`vagrant up consul2`

The client instance is started in much the same way:

`vagrant up consulclient`

The web app can be accessed at
`http://172.20.20.10/index.html`



